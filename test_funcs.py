import pytest
import coverage
import funcs


def test_deadline_score_1():
    assert funcs.deadline_score("01.09.2020","02.09.2020") == 5

def test_deadline_score_2():
    assert funcs.deadline_score("03.09.2020","02.09.2020") == 4

def test_deadline_score_3():
    assert funcs.deadline_score("10.09.2020","02.09.2020") == 3

def test_deadline_score_4():
    assert funcs.deadline_score("20.09.2020","02.09.2020") == 2

def test_deadline_score_5():
    assert funcs.deadline_score("25.09.2020","02.09.2020") == 1

def test_deadline_score_6():
    assert funcs.deadline_score("10.10.2020","02.09.2020") == 0

def test_deadline_score_7():
    assert funcs.deadline_score("asd","asd") == None

def test_late_list_1():
    assert funcs.late_list({"Иванов": "03.09.2020", "Петров": "01.09.2020"},"02.09.2020") ==  ["Иванов"]

def test_late_list_2():
    assert funcs.late_list({"Иванов": "01.09.2020", "Петров": "01.09.2020"},"02.09.2020") ==  []

def test_late_list_3():
    assert funcs.late_list({"Иванов": "03.09.2020", "Петров": "01.09.2020"},"asd") ==  None

def test_late_list_4():
    assert funcs.late_list({"Иванов": "asd", "Петров": "asd"},"02.09.2020") ==  None

def test_late_list_5():
    assert funcs.late_list({"Иванов": "asd", "Петров": "asd"},"asd") ==  None

