"""
Самарин Никита Андреевич
Группа КИ21-17/2Б ИКИТ СФУ
Работа с оценками, датами сдачи и сроками дедлайна работ
"""
from funcs import *

def main():
    """
    Выбор функции, ввод пользовательских значений, вывод результата
    """
    while True:
        func = input("Введите номер функции:")
        if func == "1":
            print("Введите дату сдачи и дату дедлайна в формате"
                  " 24.12.2013 30.12.2013:")
            try:
                pass_date, deadline_date = input().split()
            except:
                print("Некоректный ввод")
                continue
            result = deadline_score(pass_date, deadline_date)
            if result == None:
                print("Некоректный ввод")
            else:
                print(result)
        elif func == "2":
            print("""Введите словарь (ключ – фамилия студента, значение – дата сдачи)
        и дату дедлайна работы в следующем формате:
        Иванов 03.09.2020 Петров 01.09.2020 02.09.2020""")
            lst = list(input().split())
            students = dict()
            if len(lst)%2 == 1:
                for i in range(0,len(lst)-1,2):
                    students[lst[i]] = lst[i+1]
                deadline_date = lst[-1]
            else:
                print("Некоректный ввод")
                continue
            result = late_list(students, deadline_date)
            if result == None:
                print("Некоректный ввод")
            elif result == []:
                print("Опоздавших нет")
            else:
                print(result)
        else:
            print("Некоректный ввод")


main()
