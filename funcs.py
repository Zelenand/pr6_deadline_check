"""
Функции программы
"""
from datetime import *
import pytest
import coverage


def deadline_score(pass_date: str, deadline_date: str) -> int:
    """
    (pass_date: str, deadline_date: str) -> int
    Принимает две даты в строковом формате
    С помощью модуля datetime преобразует строки в даты,
    сравнивает их разницу в неделях и вычисляет оценку
    Возвращает оценку в целочисленном формате
    """
    try:
        pass_date = datetime.strptime(pass_date, "%d.%m.%Y")
        deadline_date = datetime.strptime(deadline_date, "%d.%m.%Y")
    except:
        return None
    delta = pass_date - deadline_date 
    delta = int(str(delta)[0:str(delta).find(" ")])
    if delta <= 0:
        return 5
    else:
        if delta//7 > 4:
            return 0
        return 5 - delta//7 - 1


def late_list (grades: dict, deadline_date: str) -> list[str]:
    """
    (grades: dict, deadline_date: str) -> list[str]
    Принимает принимает словарь имён-дат в строковом формате
    С помощью модуля datetime преобразует строки значений словоря grades
    и deadline_date в даты, сравнивает их, находя просроченные работы
    Возвращает список имён просрочивших в строковом формате
    """
    lates = []
    try:
        deadline_date = datetime.strptime(deadline_date, "%d.%m.%Y")
    except:
        return None
    for i in grades:
        try:
            pass_date = datetime.strptime(grades[i], "%d.%m.%Y")
        except:
            return None
        delta = pass_date - deadline_date
        delta = int(str(delta)[0:str(delta).find(" ")])
        if delta > 0:
            lates.append(i)
    return lates
